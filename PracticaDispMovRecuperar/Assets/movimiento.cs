﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class movimiento : MonoBehaviour
{

    public Rigidbody2D player;
    public Animator Movimientos;

    private void Update()
    {
        if (player.velocity.x != 0)
        {
            Movimientos.SetBool("corriendo", true);
        }
        else
        {
            Movimientos.SetBool("corriendo", false);
        }
    }

    public void Salto()
    {
        player.velocity = new Vector2(player.velocity.x, 2);
    }

    public void Iz()
    {
        player.velocity = new Vector2(-2, player.velocity.y);
        
    }

    public void Der()
    {
        player.velocity = new Vector2(2, player.velocity.y);
    }

}
