// GENERATED AUTOMATICALLY FROM 'Assets/Player.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace InputSistem
{
    public class @Inputs : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @Inputs()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""Player"",
    ""maps"": [
        {
            ""name"": ""Movimiento"",
            ""id"": ""f64b94a4-893e-4842-9222-9c8baa9529cc"",
            ""actions"": [
                {
                    ""name"": ""Izquierda"",
                    ""type"": ""Button"",
                    ""id"": ""8ec92940-f235-451d-8494-456e4b370d54"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Derecha"",
                    ""type"": ""Button"",
                    ""id"": ""260714cb-0bc6-4032-8257-e49bd6786591"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Salto"",
                    ""type"": ""Button"",
                    ""id"": ""20966244-a2f9-4814-a8e6-0982e94838c8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fad3722b-bbb1-42aa-8302-5f47d38eda86"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""388459a2-8281-4e5e-ad07-1433cf96c256"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Izquierda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5cd4d6f9-3615-4e83-b863-9b278f064e41"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""22c01d48-408c-4655-909a-5798b3e2a318"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Derecha"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4b7c914f-00cc-407f-863f-bb6b976f0953"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Salto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""298c67ac-08ad-40f2-914a-f5c44b6d7238"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Salto"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Movimiento
            m_Movimiento = asset.FindActionMap("Movimiento", throwIfNotFound: true);
            m_Movimiento_Izquierda = m_Movimiento.FindAction("Izquierda", throwIfNotFound: true);
            m_Movimiento_Derecha = m_Movimiento.FindAction("Derecha", throwIfNotFound: true);
            m_Movimiento_Salto = m_Movimiento.FindAction("Salto", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Movimiento
        private readonly InputActionMap m_Movimiento;
        private IMovimientoActions m_MovimientoActionsCallbackInterface;
        private readonly InputAction m_Movimiento_Izquierda;
        private readonly InputAction m_Movimiento_Derecha;
        private readonly InputAction m_Movimiento_Salto;
        public struct MovimientoActions
        {
            private @Inputs m_Wrapper;
            public MovimientoActions(@Inputs wrapper) { m_Wrapper = wrapper; }
            public InputAction @Izquierda => m_Wrapper.m_Movimiento_Izquierda;
            public InputAction @Derecha => m_Wrapper.m_Movimiento_Derecha;
            public InputAction @Salto => m_Wrapper.m_Movimiento_Salto;
            public InputActionMap Get() { return m_Wrapper.m_Movimiento; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(MovimientoActions set) { return set.Get(); }
            public void SetCallbacks(IMovimientoActions instance)
            {
                if (m_Wrapper.m_MovimientoActionsCallbackInterface != null)
                {
                    @Izquierda.started -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnIzquierda;
                    @Izquierda.performed -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnIzquierda;
                    @Izquierda.canceled -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnIzquierda;
                    @Derecha.started -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnDerecha;
                    @Derecha.performed -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnDerecha;
                    @Derecha.canceled -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnDerecha;
                    @Salto.started -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnSalto;
                    @Salto.performed -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnSalto;
                    @Salto.canceled -= m_Wrapper.m_MovimientoActionsCallbackInterface.OnSalto;
                }
                m_Wrapper.m_MovimientoActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Izquierda.started += instance.OnIzquierda;
                    @Izquierda.performed += instance.OnIzquierda;
                    @Izquierda.canceled += instance.OnIzquierda;
                    @Derecha.started += instance.OnDerecha;
                    @Derecha.performed += instance.OnDerecha;
                    @Derecha.canceled += instance.OnDerecha;
                    @Salto.started += instance.OnSalto;
                    @Salto.performed += instance.OnSalto;
                    @Salto.canceled += instance.OnSalto;
                }
            }
        }
        public MovimientoActions @Movimiento => new MovimientoActions(this);
        public interface IMovimientoActions
        {
            void OnIzquierda(InputAction.CallbackContext context);
            void OnDerecha(InputAction.CallbackContext context);
            void OnSalto(InputAction.CallbackContext context);
        }
    }
}
